package com.kono.SQLite;

import com.kono.SQLObjects.CatalogUpdate;

import java.sql.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Created by samue on 18/02/2017.
 */
public class SQLiteCatalog {

    private static String name;

    public SQLiteCatalog(String name) {
        SQLiteCatalog.name = name;
    }


    public static boolean createTable() throws Exception {
        boolean isCreated;

        if (SQLiteCatalog.name == null)
        {
            throw new NullPointerException("Name of Catalog must be set.");
        }

        String sql = "CREATE TABLE IF NOT EXISTS catalogues (\n"
                + "	id integer PRIMARY KEY,\n"
                + "	dt dateTime default current_timestamp,\n"
                + "	itemsComplete boolean default false,\n"
                + "	dataComplete boolean default false\n"
                + ");";

        try (Connection conn = SQLite.connect(SQLiteCatalog.name); PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.executeUpdate();
            isCreated = true;
        } catch (SQLException e) {
            isCreated = false;
        }
        return isCreated;
    }

    public static boolean createTable(String name) {
        boolean isCreated;

        String sql = "CREATE TABLE IF NOT EXISTS catalogues (\n"
                + "	id integer PRIMARY KEY,\n"
                + "	dt dateTime default current_timestamp,\n"
                + "	itemsComplete boolean default false,\n"
                + "	dataComplete boolean default false\n"
                + ");";


        try (Connection conn = SQLite.connect(name); PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.executeUpdate();
            isCreated = true;
        } catch (SQLException e) {
            isCreated = false;
        }
        return isCreated;
    }

    public boolean insertCatalog() {
        boolean isCreated;
        String sql = "INSERT INTO `catalogues`(`id`) VALUES (NULL);";

        try (Connection conn = SQLite.connect(name);
             PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.executeUpdate();
            isCreated = true;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            isCreated = false;
        }
        return isCreated;
    }

    public boolean updateItemsCatalog(boolean complete) {
        boolean isUpdated;
        String sql = "UPDATE catalogues SET itemsComplete = ?  "
                + "WHERE id = (SELECT id FROM catalogues ORDER BY id DESC LIMIT 1)";

        try (Connection conn = SQLite.connect(name); PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setBoolean(1, complete);
            pstmt.executeUpdate();
            isUpdated = true;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            isUpdated = false;
        }
        return isUpdated;
    }
    public boolean updateDataCatalog(boolean complete) {
        boolean isUpdated;
        String sql = "UPDATE catalogues SET dataComplete = ?  "
                + "WHERE id = (SELECT id FROM catalogues ORDER BY id DESC LIMIT 1)";

        try (Connection conn = SQLite.connect(name); PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setBoolean(1, complete);
            pstmt.executeUpdate();
            isUpdated = true;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            isUpdated = false;
        }
        return isUpdated;
    }


    public CatalogUpdate getCatalog() {
        String sql = "SELECT * FROM catalogues ORDER BY id DESC LIMIT 1";
        CatalogUpdate latest = new CatalogUpdate();
        try (Connection conn = SQLite.connect(name);
             Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery(sql)) {

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

            while (rs.next()) {
                latest.setId(rs.getInt("id"));
                latest.setDt( LocalDateTime.parse(rs.getString("dt"), formatter));
                latest.setItemsComplete(rs.getBoolean("itemsComplete"));
                latest.setDataComplete(rs.getBoolean("dataComplete"));

            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return latest;
    }
}
