package com.kono.SQLite;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by samue on 19/02/2017.
 */
public class SQLite {

    /**
     * Connect to the @name.db database
     *
     * @param name the name of the database.
     * @return the Connection object
     */
    public static Connection connect(String name) throws SQLException {
        // SQLiteCatalog connection string
        String url = "jdbc:sqlite:" + name + ".db";

        return DriverManager.getConnection(url);
    }

}
