package com.kono.SQLite;

import com.kono.Objects.Items.Item;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

/**
 * Created by samue on 19/02/2017.
 */
public class SQLiteItem {
    private static String name;

    public SQLiteItem(String name) {
        SQLiteItem.name = name;
    }

    public static boolean createTable(String name) {
        boolean isCreated;

        String sql = "CREATE TABLE IF NOT EXISTS items (\n"
                + "	id integer PRIMARY KEY,\n"
                + "	dt dateTime default current_timestamp,\n"
                + "	name String  NOT NULL,\n"
                + "	icon String  NOT NULL,\n"
                + "	type String  NOT NULL,\n"
                + "	description String  NOT NULL,\n"
                + "	members boolean default false\n"
                + ");";

        try (Connection conn = SQLite.connect(name); PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.executeUpdate();
            isCreated = true;
        } catch (SQLException e) {
            isCreated = false;
        }
        return isCreated;
    }

    public static boolean createTable() {
        boolean isCreated;

        if (SQLiteItem.name == null) {
            throw new NullPointerException("Name of Catalog must be set.");
        }


        String sql = "CREATE TABLE IF NOT EXISTS items (\n"
                + "	id integer PRIMARY KEY,\n"
                + "	dt dateTime default current_timestamp,\n"
                + "	name String  NOT NULL,\n"
                + "	icon String  NOT NULL,\n"
                + "	type String  NOT NULL,\n"
                + "	description String  NOT NULL,\n"
                + "	members boolean default false,\n"
                + "UNIQUE(id));";

        try (Connection conn = SQLite.connect(name); PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.executeUpdate();
            isCreated = true;
        } catch (SQLException e) {
            isCreated = false;
        }
        return isCreated;
    }

    public boolean insertItem(int id, String itemName, String iconURL, String type, String description, boolean isMembers) {
        boolean isCreated = false;

        String sql = "INSERT INTO items(id,name,icon,type,description,members) VALUES (?,?,?,?,?,?);";


        try (Connection conn = SQLite.connect(name);
             PreparedStatement pstmt = conn.prepareStatement(sql)) {

            URL url = new URL(iconURL);
            BufferedImage image = ImageIO.read(url);
            ByteArrayOutputStream output = new ByteArrayOutputStream();
            ImageIO.write(image, "png", output);


            pstmt.setInt(1, id);
            pstmt.setString(2, itemName);
            pstmt.setString(3, Base64.getEncoder().encodeToString(output.toByteArray()));
            pstmt.setString(4, type);
            pstmt.setString(5, description);
            pstmt.setBoolean(6, isMembers);
            pstmt.setInt(7, id);
            pstmt.executeUpdate();
            isCreated = true;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            isCreated = false;
        } catch (IOException ignored) {
        }

        return isCreated;
    }

    public boolean insertItem(Item item) {
        boolean isCreated = false;
        String sql = "INSERT INTO items(id,name,icon,type,description,members) VALUES (?,?,?,?,?,?);";


        try (Connection conn = SQLite.connect(name);
             PreparedStatement pstmt = conn.prepareStatement(sql)) {

            URL url = new URL(item.getIconLarge());
            BufferedImage image = ImageIO.read(url);
            ByteArrayOutputStream output = new ByteArrayOutputStream();
            ImageIO.write(image, "png", output);

            pstmt.setInt(1, item.getId());
            pstmt.setString(2, item.getName());
            pstmt.setString(3, Base64.getEncoder().encodeToString(output.toByteArray()));
            pstmt.setString(4, item.getType());
            pstmt.setString(5, item.getDescription());
            pstmt.setBoolean(6, item.getMembers());
            pstmt.executeUpdate();
            isCreated = true;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            isCreated = false;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return isCreated;
    }

    public boolean updateItem(int id, String itemName, String iconURL, String type, String description, boolean isMembers) {
        boolean isUpdated;
        String sql = "UPDATE items Set name = ? ,icon = ? , iconURL = ? , type = ? , description = ? , members = ? WHERE id = ?";

        try (Connection conn = SQLite.connect(name);
             PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setInt(6, id);
            pstmt.setString(1, itemName);
            pstmt.setString(2, iconURL);
            pstmt.setString(3, type);
            pstmt.setString(4, description);
            pstmt.setBoolean(5, isMembers);
            pstmt.executeUpdate();
            isUpdated = true;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            isUpdated = false;
        }
        return isUpdated;
    }

    public List<Integer> getItemIds() {
        String sql = "SELECT id FROM items";

        List<Integer> ids = new ArrayList<>();

        try (Connection conn = SQLite.connect(name);
             PreparedStatement pstmt = conn.prepareStatement(sql)) {

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                ids.add(rs.getInt("id"));
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return ids;
    }

    public Item getItem(int id) {
        String sql = "SELECT * FROM items WHERE  id = ?";

        Item item = new Item();

        try (Connection conn = SQLite.connect(name);
             PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setInt(1, id);

            ResultSet rs = pstmt.executeQuery();
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

            while (rs.next()) {
                item.setId(rs.getInt("id"));
                item.setDt(LocalDateTime.parse(rs.getString("dt"), formatter));
                item.setIconLarge(rs.getString("icon"));
                item.setName(rs.getString("name"));
                item.setDescription(rs.getString("description"));
                item.setMembers(rs.getBoolean("members"));
                item.setType(rs.getString("type"));
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return item;
    }
}
