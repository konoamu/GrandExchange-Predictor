package com.kono.SQLite;

import com.kono.Objects.Items.Item;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Created by samue on 19/02/2017.
 */
public class SQLiteData {
    private static String name;

    public SQLiteData(String name) {
        SQLiteData.name = name;
    }

    public static boolean createTable(String name) {
        boolean isCreated;

        String sql = "CREATE TABLE IF NOT EXISTS items (\n"
                + "	id integer PRIMARY KEY AUTOINCREMENT,\n"
                + "	dt dateTime NOT NULL,\n"
                + "	price integer  NOT NULL,\n"
                + " itemId integer NOT NULL,\n"
                + " FOREIGN KEY(itemId) REFERENCES items(id),\n"
                + " UNIQUE(dt));";

        try (Connection conn = SQLite.connect(name); PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.executeUpdate();
            isCreated = true;
        } catch (SQLException e) {
            isCreated = false;
        }
        return isCreated;
    }

    public static boolean createTable() {
        boolean isCreated;

        if (SQLiteData.name == null) {
            throw new NullPointerException("Name of Catalog must be set.");
        }

        String sql = "CREATE TABLE IF NOT EXISTS data (\n"
                + "	id integer PRIMARY KEY AUTOINCREMENT,\n"
                + "	dt dateTime NOT NULL,\n"
                + "	price integer  NOT NULL,\n"
                + " itemId integer NOT NULL,\n"
                + " FOREIGN KEY(itemId) REFERENCES items(id));";

        try (Connection conn = SQLite.connect(name); PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.executeUpdate();
            isCreated = true;
        } catch (SQLException e) {
            isCreated = false;
        }
        return isCreated;
    }

    public boolean insertPrice(int itemId, LocalDateTime dt, int price) {
        boolean isCreated = false;
        String sql = "INSERT INTO data(itemId,dt,price) VALUES (?,?,?);";

        try (Connection conn = SQLite.connect(name);
             PreparedStatement pstmt = conn.prepareStatement(sql)) {

            DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

            pstmt.setInt(1, itemId);
            pstmt.setString(2, dt.format(DATE_TIME_FORMATTER));
            pstmt.setInt(3, price);
            pstmt.executeUpdate();
            isCreated = true;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            isCreated = false;
        }
        return isCreated;
    }

    public Item getItem(int id) {
        String sql = "SELECT * FROM items WHERE itemId = ?";

        Item item = new Item();

        try (Connection conn = SQLite.connect(name);
             PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setInt(1, id);

            ResultSet rs = pstmt.executeQuery();
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

            while (rs.next()) {
                item.setId(rs.getInt("id"));
                item.setDt(LocalDateTime.parse(rs.getString("dt"), formatter));
                item.setIconLarge(rs.getString("icon"));
                item.setName(rs.getString("name"));
                item.setDescription(rs.getString("description"));
                item.setMembers(rs.getBoolean("members"));
                item.setType(rs.getString("type"));
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return item;
    }
}
