package com.kono;

import com.kono.Objects.Catalogue.Catalogue;
import com.kono.Objects.Catalogue.CategoryName;
import com.kono.Objects.Catalogue.Categorys;
import com.kono.Objects.Items.Graph.Graph;
import com.kono.Objects.Items.Item;
import com.kono.Objects.Items.Items;
import com.kono.SQLite.SQLiteCatalog;
import com.kono.SQLite.SQLiteData;
import com.kono.SQLite.SQLiteItem;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Created by samue on 18/02/2017.
 */
public class main {

    public static void main(String[] args) throws Exception {

        final SQLiteCatalog sqLiteCatalog = new SQLiteCatalog("Test");
        SQLiteCatalog.createTable();

        final SQLiteItem sqLiteItem = new SQLiteItem("Test");
        SQLiteItem.createTable();

        final SQLiteData sqLiteData = new SQLiteData("Test");
        SQLiteData.createTable();

        Request request = new Request();
        LocalDateTime localDateTimeSnapshot = sqLiteCatalog.getCatalog().getDt();
        LocalDateTime now = LocalDateTime.now();

        if (localDateTimeSnapshot == null) {
            localDateTimeSnapshot = LocalDateTime.MIN;
        }

        long between = ChronoUnit.DAYS.between(localDateTimeSnapshot, now);
        if (between >= 7L) {
            sqLiteCatalog.insertCatalog();
            for (CategoryName categoryName : CategoryName.values()) {
                System.out.println(categoryName.name());
                Optional catalog = null;
                try {
                    catalog = request.get(new URI("http://services.runescape.com/m=itemdb_rs/api/catalogue/category.json?category=" + categoryName.getValue()), Catalogue.class);
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                }
                if (catalog != null && catalog.isPresent() && catalog.get() instanceof Catalogue) {
                    List<Categorys> categorys = ((Catalogue) catalog.get()).getCategorys();
                    for (final Categorys cat : categorys) {
                        int pages = (int) Math.ceil(cat.getItems() / 12);
                        for (int page = 0; page < pages; page++) {
                            System.out.print("Letter: " + cat.getLetter() + " page: " + page + " of " + pages);
                            URI url = null;
                            try {
                                url = getItemURL(categoryName, cat, page);
                            } catch (URISyntaxException | UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                            Optional itemPage = request.get(url, Items.class);
                            if (itemPage.isPresent() && itemPage.get() instanceof Items) {
                                List<Item> items = ((Items) itemPage.get()).getItems();
                                for (Item item : items) {
                                    if (sqLiteItem.getItem(item.getId()).getId() == null) {
                                        sqLiteItem.insertItem(item);
                                    }

                                    //get price data?
                                    if (args.length > 0 && args[0].equals("true")) {
                                        insertGraphData(sqLiteData,sqLiteItem, request, item.getId());
                                    }
                                }
                            }
                            System.out.print("\r");
                        }
                    }
                }

                if (categoryName.getValue() == 37) {
                    sqLiteCatalog.updateItemsCatalog(true);
                }
            }
        } else {
            System.out.println((7 - between) + " days remain till next update.");
            sqLiteItem.getItemIds().forEach(id -> insertGraphData(sqLiteData,sqLiteItem, request, id));

        }
    }

    private static void insertGraphData(SQLiteData sqLiteData,SQLiteItem sqLiteItem, Request request, int id) {
        System.out.println("Getting: "+ sqLiteItem.getItem(id).getName());
        URI itemGraphURL = getItemGraphURL(id);
        Optional graph = request.get(itemGraphURL, Graph.class);

        if (graph.isPresent() && graph.get() instanceof Graph) {
            Graph data = (Graph) graph.get();

            for (Map.Entry<Long, Integer> entry : data.getDaily().entrySet()) {
                Long date = entry.getKey();
                Integer price = entry.getValue();
                if (price != 0) {
                    sqLiteData.insertPrice(id, Instant.ofEpochMilli(date).atZone(ZoneId.systemDefault()).toLocalDateTime(), price);
                }
            }

            for (Map.Entry<Long, Integer> entry : data.getAverage().entrySet()) {
                Long date = entry.getKey();
                Integer price = entry.getValue();
                if (price != 0) {
                    sqLiteData.insertPrice(id, Instant.ofEpochMilli(date).atZone(ZoneId.systemDefault()).toLocalDateTime(), price);
                }
            }
        }
    }

    private static URI getItemGraphURL(Integer id) {
        String url = "http://services.runescape.com/m=itemdb_rs/api/graph/{id}.json";
        url = url.replace("{id}", Integer.toString(id));
        return URI.create(url);
    }

    private static URI getItemURL(CategoryName categoryName, Categorys cat, int page) throws URISyntaxException, UnsupportedEncodingException {

        String url = "http://services.runescape.com/m=itemdb_rs/api/catalogue/items.json?category={X}&alpha={Y}&page={Z}";
        url = url.replace("{X}", Integer.toString(categoryName.getValue()));
        url = url.replace("{Y}", URLEncoder.encode(cat.getLetter(), "UTF-8"));
        url = url.replace("{Z}", Integer.toString(page));

        return URI.create(url);
    }
}
