package com.kono.SQLObjects;

import java.time.LocalDateTime;

/**
 * Created by samue on 19/02/2017.
 */
public class CatalogUpdate {

    private int id;
    private LocalDateTime dt;
    private boolean itemsComplete;
    private boolean dataComplete;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocalDateTime getDt() {
        return dt;
    }

    public void setDt(LocalDateTime dt) {
        this.dt = dt;
    }

    public boolean isItemsComplete() {
        return itemsComplete;
    }

    public void setItemsComplete(boolean itemsComplete) {
        this.itemsComplete = itemsComplete;
    }

    public boolean isDataComplete() {
        return dataComplete;
    }

    public void setDataComplete(boolean dataComplete) {
        this.dataComplete = dataComplete;
    }

    @Override
    public String toString() {
        return "CatalogUpdate{" +
                "id=" + id +
                ", dt=" + dt +
                ", itemsComplete=" + itemsComplete +
                ", dataComplete=" + dataComplete +
                '}';
    }
}
