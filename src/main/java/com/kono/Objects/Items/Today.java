
package com.kono.Objects.Items;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Today {

    @SerializedName("trend")
    @Expose
    private String trend;
    @SerializedName("price")
    @Expose
    private String priceString;

    public String getTrend() {
        return trend;
    }

    public void setTrend(String trend) {
        this.trend = trend;
    }

    public Double getPrice() {
        Double value = -1.0;
        boolean isNegative = false;




        if (!priceString.equals("0")) {
            if (priceString.contains("-")){
                isNegative = true;
                priceString = priceString.replace("-", "");
            }
            value = parsePrice(value);
        }else {
            priceString = "untradable";
        }
        if (isNegative){
            value *= -1;
        }
        return value;
    }

    private Double parsePrice(Double value) {

        if (priceString.contains("k")) {
            String v = priceString.replace("k", "");
            value = Double.parseDouble(v) * 1000;
        } else if (priceString.contains("m")) {
            String v = priceString.replace("m", "");
            value = Double.parseDouble(v) * 1000000;
        } else if (priceString.contains("b")) {
            String v = priceString.replace("b", "");
            value = Double.parseDouble(v) * 1000000000;
        }else {
            String v = priceString.replace(",", "");
            value = Double.parseDouble(priceString);
        }
        return value;
    }

    public void setPrice(String price) {
        this.priceString = price;
    }

}