package com.kono.Objects.Items;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.kono.Objects.Items.Graph.Graph;

import java.time.LocalDateTime;

public class Item {

    @SerializedName("icon")
    @Expose
    private String icon;
    @SerializedName("icon_large")//
    @Expose
    private String iconLarge;
    @SerializedName("id")//
    @Expose
    private Integer id;
    @SerializedName("type")//
    @Expose
    private String type;
    @SerializedName("typeIcon")
    @Expose
    private String typeIcon;
    @SerializedName("name")//
    @Expose
    private String name;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("current")
    @Expose
    private Current current;
    @SerializedName("today")
    @Expose
    private Today today;
    @SerializedName("members")
    @Expose
    private Boolean members;

    private LocalDateTime dt;
    private Graph graph;

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getIconLarge() {
        return iconLarge;
    }

    public void setIconLarge(String iconLarge) {
        this.iconLarge = iconLarge;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTypeIcon() {
        return typeIcon;
    }

    public void setTypeIcon(String typeIcon) {
        this.typeIcon = typeIcon;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Current getCurrent() {
        return current;
    }

    public void setCurrent(Current current) {
        this.current = current;
    }

    public Today getToday() {
        return today;
    }

    public void setToday(Today today) {
        this.today = today;
    }

    public Boolean getMembers() {
        return members;
    }

    public void setMembers(Boolean members) {
        this.members = members;
    }

    public LocalDateTime getDt() {
        return dt;
    }

    public void setDt(LocalDateTime dt) {
        this.dt = dt;
    }

    public Graph getGraph() {
        return graph;
    }

    public void setGraph(Graph graph) {
        this.graph = graph;
    }

    @Override
    public String toString() {
        return "Item{" +
                "icon='" + iconLarge + '\'' +
                ", id=" + id +
                ", type='" + type + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", current=" + current +
                ", today=" + today +
                ", members=" + members +
                ", dt=" + dt +
                '}';
    }
}