package com.kono.Objects.Items.Graph;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.HashMap;

/**
 * Created by samue on 18/02/2017.
 */
public class Graph {
    @SerializedName("daily")
    @Expose
    private HashMap<Long, Integer> daily;
    @SerializedName("average")
    @Expose
    private HashMap<Long, Integer> average;

    public HashMap<Long, Integer> getDaily() {
        return daily;
    }

    public HashMap<Long, Integer> getAverage() {
        return average;
    }

    @Override
    public String toString() {
        return "Graph{" +
                "daily=" + daily +
                ", average=" + average +
                '}';
    }
}
