package com.kono.Objects.Catalogue;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by samue on 18/02/2017.
 */
public class Categorys {
    @SerializedName("letter")
    @Expose
    private String letter;
    @SerializedName("items")
    @Expose
    private int items;

    public String getLetter() {
        return letter;
    }

    public void setLetter(String letter) {
        this.letter = letter;
    }

    public int getItems() {
        return items;
    }

    public void setItems(int items) {
        this.items = items;
    }
}
