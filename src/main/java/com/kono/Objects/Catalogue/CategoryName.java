package com.kono.Objects.Catalogue;

/**
 * Created by samue on 18/02/2017.
 */
public enum CategoryName {

    Miscellaneous(0),
    Ammo(1),
    Arrows(2),
    Bolts(3),
    ConstructionMaterials(4),
    ConstructionProjects(5),
    CookingIngredients(6),
    Costumes(7),
    CraftingMaterials(8),
    Familiars(9),
    FarmingProduce(10),
    FletchingMaterials(11),
    FoodAndDrink(12),
    HerbloreMaterials(13),
    HuntingEquipment(14),
    HuntingProduce(15),
    Jewellery(16),
    MageArmour(17),
    MageWeapons(18),
    MeleeArmourLow(19),
    MeleeArmourMid(20),
    MeleeArmourHigh(21),
    MeleeWeaponsLow(22),
    MeleeWeaponsMid(23),
    MeleeWeaponsHigh(24),
    MiningAndSmithing(25),
    Potions(26),
    PrayerArmour(27),
    PrayerMaterials(28),
    RangeArmour(29),
    RangeWeapons(30),
    Runecrafting(31),
    RunesSpellsAndTeleports(32),
    Seeds(33),
    SummoningScrolls(34),
    ToolsAndContainers(35),
    WoodcuttingProduct(36),
    PocketItems(37);

    int value;
    CategoryName(int v){
        value = v;
    }
    public int getValue(){
        return value;
    }
}
