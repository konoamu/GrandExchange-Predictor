package com.kono.Objects.Catalogue;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by samue on 18/02/2017.
 */
public class Catalogue {
    @SerializedName("alpha")
    @Expose
    private List<Categorys> alpha = null;

    public List<Categorys> getCategorys() {
        return alpha;
    }

    public void setCategorys(List<Categorys> alpha) {
        this.alpha = alpha;
    }
}
