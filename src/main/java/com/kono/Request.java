package com.kono;
/**
 * Created by samue on 18/02/2017.
 */

import com.google.gson.Gson;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

import java.net.URI;
import java.util.Optional;

public class Request {

    public Optional get(URI url, Class<?> aClass) {
        Optional<Object> response = Optional.empty();
        String json = "";
        try (CloseableHttpClient httpClient = HttpClientBuilder.create().build()) {
            int count = 0;
            while (json.equals("")) {
                if (count > 0) {
                    Thread.sleep(60000); //wait 60 seconds we have requested to much.
                    System.out.print("                             \r");
                }
                HttpGet request = new HttpGet(url);
                HttpResponse result = httpClient.execute(request);
                json = EntityUtils.toString(result.getEntity(), "UTF-8");

                Gson gson = new Gson();
                response = Optional.ofNullable(gson.fromJson(json, aClass));
                count++;

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }
}
